'use strict';

const http = require('http');
const { URL } = require('url');
const { exec } = require('child_process');

const port = process.env.PORT;
const sitesBase = process.env.SITESBASE;
const nodePath = process.env.NODEPATH;

const server = http.createServer((req, res) => {
  const host = (req.connection.encrypted ? 'https://' : 'http://') + req.headers['host'];
  const reqUrl = new URL(req.url, host);
  if (reqUrl.pathname === '/build-site') {
    const cmd = `cd ${sitesBase}/${reqUrl.searchParams.get('site')}; node --max-old-space-size=512 node_modules/.bin/gatsby build`;
    exec(cmd, {
      env: { 'PATH': nodePath }
    }, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
      } else {
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
      }

      res.end();
    });
  } else {
    res.end();
  }
});

server.listen(port, () => {
  console.log(`Server running at ${port}`);
});